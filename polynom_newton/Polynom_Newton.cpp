#include "stdafx.h"
#include "Polynom_Newton.h"


Polynom_Newton::Polynom_Newton(int degree, double * grindNotes, double * valuesOfFunction, int quantityIntervals, double * valuesResultsGrind, bool analyticExpression) {
	n = degree;

	grind = false;

	x = new double[n];
	for (int i = 0; i < n; i++) {
		x[i] = grindNotes[i];
	}

	y = new double[n];
	for (int i = 0; i < n; i++) {
		y[i] = valuesOfFunction[i];
	}

	m = quantityIntervals;

	xm = new double[m];
	for (int i = 0; i < m; i++) {
		xm[i] = valuesResultsGrind[i];
	}

	t = analyticExpression;
}

Polynom_Newton::Polynom_Newton(int degree, int begin, int end, double * valuesOfFunction, int quantityIntervals, double * valuesResultingGrind, bool analyticExpression) {
}

Polynom_Newton::~Polynom_Newton() {
}
