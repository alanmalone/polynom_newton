#include "DataManipulator.h";

#pragma once
class Polynom_Newton {
	//����� f() - ��� ������������� �������, ���������������, ���� ������� �� ��������
private:
	int n; //������� ��������
	bool grind; //����� ����������� - true, ������������� false
	int a, b; //������� �������
	double* x; //���� �����
	double* y; //�������� ������� � ����� �����
	int m; //���������� ���������� � �������������� �����
	double* xm; //���� �������������� �����
	bool t; //�������� ��� ��� ������������� ��������� ��� ������� f(x), true - ��������, false - �� ��������
public:
	Polynom_Newton(int degree, double * grindNotes, double * valuesOfFunction, int quantityIntervals, double * valuesResultsGrind, bool analyticExpression); //��� ������������� �����
	Polynom_Newton(int degree, int begin, int end, double * valuesOfFunction, int quantityIntervals, double * valuesResultingGrind, bool analyticExpression); //��� ����������� �����
	void calculate_polynom();
	void firstDerivative();
	void secondDerivative();
	~Polynom_Newton();
};

