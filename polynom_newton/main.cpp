// polynom_newton.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "Polynom_Newton.h";
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

ofstream out;

int main() {
	setlocale(LC_ALL, "Russian");
	int degree;
	int task;
	int quantityIntervals;

	ifstream in("unevenIn.txt");
	in >> task >> degree;

	/*
	cout << degree << endl;
	cout << task << endl;
	*/
	
	double* grindNotes = new double[degree];
	for (int i = 0; i < degree; i++) {
		in >> grindNotes[i];
		//cout << grindNotes[i] << " ";
	}
	
	double* valuesOfFunction = new double[degree];
	for (int i = 0; i < degree; i++) {
		in >> valuesOfFunction[i];
		//cout << valuesOfFunction[i] << " ";
	}

	in >> quantityIntervals;
	//cout << quantityIntervals;

	double* valuesResultsGrind = new double[quantityIntervals];
	for (int i = 0; i < quantityIntervals; i++) {
		in >> valuesResultsGrind[i];
		//cout << valuesResultsGrind[i] << " ";
	}

	Polynom_Newton polynom1(degree, grindNotes, valuesOfFunction, quantityIntervals, valuesResultsGrind, true);
    return 0;
}

